﻿using Entity;
using Infrastructure.Entity;
using Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Repositories
{
    public class ItemRepository : IItemRepository
    {
        private readonly ApplicationDbContext _context;

        private readonly IServiceProvider _serviceProvider;

        public ItemRepository(ApplicationDbContext context, IServiceProvider serviceProvider)
        {
            _context = context;
            _serviceProvider = serviceProvider;
        }
        public async Task<IItem> CreateAsync(string name, IGroceryList groceryList)
        {
            var entity = _serviceProvider.GetRequiredService<IItem>();
            entity.Id = Guid.NewGuid();
            entity.Name = name;
            entity.GroceryList = groceryList;
            entity.IsDone = false;

            var item = await _context.Item.AddAsync((ItemEntity)entity);

            await _context.SaveChangesAsync();

            return item.Entity;
        }

        public async Task<bool> DeleteAsync(IItem item)
        {
            var entity = await _context.Item.FindAsync(item.Id);

            if (entity is not null)
            {
                _context.Item.Remove(entity);

                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }

        public async Task<IItem?> ReadAsync(Guid id)
        {
            return await _context.Item.FindAsync(id);
        }

        public async Task<IEnumerable<IItem>> ReadCollectionAsync(IGroceryList groceryList)
        {
            return await _context.Item
                .Where(i => i.GroceryList.Equals(groceryList))
                .ToListAsync();
        }

        public async Task<IItem> UpdateAsync(IItem item, string name)
        {
            item.Name = name;

            await _context.SaveChangesAsync();

            return item;
        }

        public async Task<IItem> UpdateAsync(IItem item, bool isDone)
        {
            item.IsDone = isDone;

            await _context.SaveChangesAsync();

            return item;
        }
    }
}
