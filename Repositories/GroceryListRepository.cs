﻿using Entity;
using Infrastructure.Entity;
using Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Xml.Linq;

namespace Repositories
{
    public class GroceryListRepository: IGroceryListRepository
    {
        private readonly ApplicationDbContext _context;

        private readonly IServiceProvider _serviceProvider;

        public GroceryListRepository(ApplicationDbContext context, IServiceProvider serviceProvider)
        {
            _context = context;
            _serviceProvider = serviceProvider;
        }
        public async Task<IGroceryList> CreateAsync(string name, IUser user)
        {
            var entity = _serviceProvider.GetRequiredService<IGroceryList>();
            entity.Id = Guid.NewGuid();
            entity.Name = name;
            entity.User = user;
            entity.IsDeleted = false;

            var groceryList = await _context.GroceryList.AddAsync((GroceryListEntity)entity);

            await _context.SaveChangesAsync();

            return groceryList.Entity;
        }

        public async Task<IGroceryList?> ReadAsync(Guid id, IUser user, bool isDeleted = false)
        {
            return await _context.GroceryList
                .Include(gl => gl.User)
                .FirstOrDefaultAsync(gl => gl.Id.Equals(id) && gl.User.Equals(user) && gl.IsDeleted == isDeleted);
        }

        public async Task<IEnumerable<IGroceryList>> ReadCollectionAsync(IUser user, bool isDeleted = false)
        {
            return await _context.GroceryList
                .Where(gl => gl.User.Equals(user) && gl.IsDeleted == isDeleted)
                .ToListAsync();
        }

        public async Task<IGroceryList> UpdateAsync(IGroceryList groceryList, string name)
        {
            groceryList.Name = name;

            await _context.SaveChangesAsync();

            return groceryList;
        }

        public async Task<IGroceryList> UpdateAsync(IGroceryList groceryList, bool isDeleted)
        {
            groceryList.IsDeleted = isDeleted;

            await _context.SaveChangesAsync();

            return groceryList;
        }
    }
}
