﻿using Entity;
using Microsoft.EntityFrameworkCore;

namespace Repositories
{
    public class ApplicationDbContext: DbContext 
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        { }
        public DbSet<UserEntity> User { get; set; }
        public DbSet<UserRefreshTokenEntity> UserRefreshToken { get; set; }
        public DbSet<GroceryListEntity> GroceryList { get; set; }
        public DbSet<ItemEntity> Item { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserRefreshTokenEntity>()
                .HasOne(urt => urt.User as UserEntity);

            modelBuilder.Entity<GroceryListEntity>()
                .HasOne(gl => gl.User as UserEntity);

            modelBuilder.Entity<ItemEntity>()
                .HasOne(i => i.GroceryList as GroceryListEntity);
        }

    }
}
