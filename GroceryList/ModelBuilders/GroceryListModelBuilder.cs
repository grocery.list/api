﻿using GroceryList.Models;
using Infrastructure.Entity;

namespace GroceryList.ModelBuilders
{
    public class GroceryListModelBuilder
    {
        public IEnumerable<ListModel> Build(IEnumerable<IGroceryList> groceryLists)
        {
            var listModels = groceryLists.Select(Build);

            return listModels;
        }
        public ListModel Build(IGroceryList groceryList)
        {
            return new ListModel
            {
                Id = groceryList.Id,
                Name = groceryList.Name
            };
        }
    }
}
