﻿using Newtonsoft.Json;

namespace GroceryList.Models
{
    public class ListModel
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
