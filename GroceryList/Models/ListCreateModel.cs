﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace GroceryList.Models
{
    public class ListCreateModel
    {
        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
