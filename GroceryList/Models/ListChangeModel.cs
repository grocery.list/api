﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace GroceryList.Models
{
    public class ListChangeModel
    {
        [Required]
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
