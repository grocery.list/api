﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace GroceryList.Models
{
    public class ListDeleteModel
    {
        [Required]
        [JsonProperty("id")]
        public Guid Id { get; set; }
    }
}
