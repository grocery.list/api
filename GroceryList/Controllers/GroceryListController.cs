﻿using GroceryList.ModelBuilders;
using GroceryList.Models;
using Infrastructure.Repositories;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GroceryList.Controllers
{
    public class GroceryListController: ControllerBase
    {
        private readonly IGroceryListRepository _groceryListRepository;
        private readonly IUserService _userService;
        private readonly GroceryListModelBuilder _groceryListModelBuilder;
        public GroceryListController(IGroceryListRepository groceryListRepository, IUserService userService, GroceryListModelBuilder groceryListModelBuilder)
        {
            _groceryListRepository = groceryListRepository;
            _userService = userService;
            _groceryListModelBuilder = groceryListModelBuilder;
        }

        [Authorize]
        [HttpPost("list")]
        public async Task<IActionResult> CreateList(ListCreateModel model) 
        {
            var user = _userService.CurrentUser;

            if(model.Name.Length == 0 || model.Name.Length > 255 || user is null)
                return BadRequest();

            var groceryList = await _groceryListRepository.CreateAsync(model.Name, user);

            var result = _groceryListModelBuilder.Build(groceryList);

            return Ok(result);
        }

        [Authorize]
        [HttpGet("list")]
        public async Task<IActionResult> GetUserLists()
        {
            var user = _userService.CurrentUser;

            if(user is null) return BadRequest();

            var groceryListCollection = await _groceryListRepository.ReadCollectionAsync(user);

            var result = _groceryListModelBuilder.Build(groceryListCollection);

            return Ok(result);
        }

        [Authorize]
        [HttpPut("list")]
        public async Task<IActionResult> ChangeList(ListChangeModel model)
        {
            var user = _userService.CurrentUser;
            if(user is null) return BadRequest();

            var groceryList = await _groceryListRepository.ReadAsync(model.Id, user);

            if(groceryList is null || model.Name.Length == 0 || model.Name.Length > 255)
                return BadRequest();

            groceryList = await _groceryListRepository.UpdateAsync(groceryList, model.Name);

            var result = _groceryListModelBuilder.Build(groceryList);

            return Ok(result);
        }

        [Authorize]
        [HttpDelete("list")]
        public async Task<IActionResult> DeleteList(ListDeleteModel model)
        {
            var user = _userService.CurrentUser;
            if (user is null) return BadRequest();

            var groceryList = await _groceryListRepository.ReadAsync(model.Id, user);

            if (groceryList is null)
                return BadRequest();

            groceryList = await _groceryListRepository.UpdateAsync(groceryList, isDeleted: true);

            return Ok();
        }
    }
}
