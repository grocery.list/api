﻿using Infrastructure.Entity;
using Infrastructure.Models;

namespace Infrastructure.Services
{
    public interface IUserService
    {
        IUser CurrentUser { get; }

        Task<TokenModel?> CreateAsync(string username, string password);

        Task<TokenModel?> AuthenticateAsync(string username, string password);

        Task<TokenModel?> RefreshTokenAsync(TokenModel model);
    }
}
