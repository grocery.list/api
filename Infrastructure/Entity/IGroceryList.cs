﻿namespace Infrastructure.Entity
{
    public interface IGroceryList
    {
        Guid Id { get; set; }
        string Name { get; set; }
        IUser User { get; set; }
        bool IsDeleted { get; set; }
    }
}
