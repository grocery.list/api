﻿namespace Infrastructure.Entity
{
    public interface IItem
    {
        Guid Id { get; set; }
        string Name { get; set; }
        bool IsDone { get; set; }
        IGroceryList GroceryList { get; set; }
    }
}
