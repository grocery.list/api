﻿using Infrastructure.Entity;

namespace Infrastructure.Repositories
{
    public interface IItemRepository
    {
        Task<IItem> CreateAsync(string name, IGroceryList groceryList);
        Task<IEnumerable<IItem>> ReadCollectionAsync(IGroceryList groceryList);
        Task<IItem?> ReadAsync(Guid id);
        Task<IItem> UpdateAsync(IItem item, string name);
        Task<IItem> UpdateAsync(IItem item, bool isDone);
        Task<bool> DeleteAsync(IItem item);
    }
}
