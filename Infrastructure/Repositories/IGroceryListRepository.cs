﻿using Infrastructure.Entity;

namespace Infrastructure.Repositories
{
    public interface IGroceryListRepository
    {
        Task<IGroceryList> CreateAsync(string name, IUser user);
        Task<IEnumerable<IGroceryList>> ReadCollectionAsync(IUser user, bool isDeleted = false);
        Task<IGroceryList?> ReadAsync(Guid id, IUser user, bool isDeleted = false);
        Task<IGroceryList> UpdateAsync(IGroceryList groceryList, string name);
        Task<IGroceryList> UpdateAsync(IGroceryList groceryList, bool isDeleted);
    }
}
