﻿using Infrastructure.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity
{
    public class ItemEntity: IItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsDone { get; set; }

        [ForeignKey("GroceryList")]
        public Guid GroceryListFk { get; set; }
        public IGroceryList GroceryList { get; set; }
    }
}
