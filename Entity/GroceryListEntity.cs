﻿using Infrastructure.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity
{
    public class GroceryListEntity: IGroceryList 
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        [ForeignKey("User")]
        public Guid UserFk { get; set; }
        public IUser User { get; set; }
        public bool IsDeleted { get; set; }
    }
}
