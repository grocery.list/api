﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Item.Models
{
    public class ItemChangeCompletionModel
    {
        [Required]
        [JsonProperty("listId")]
        public Guid ListId { get; set; }

        [Required]
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [Required]
        [JsonProperty("completion")]
        public bool IsDone { get; set; }
    }
}
