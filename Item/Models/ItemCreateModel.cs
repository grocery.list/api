﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Item.Models
{
    public class ItemCreateModel
    {
        [Required]
        [JsonProperty("listId")]
        public Guid ListId { get; set; }

        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
