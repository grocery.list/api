﻿using Newtonsoft.Json;

namespace Item.Models
{
    public class ItemModel
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("isDone")]
        public bool IsDone { get; set; }
    }
}
