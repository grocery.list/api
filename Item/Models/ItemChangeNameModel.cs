﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Item.Models
{
    public class ItemChangeNameModel
    {
        [Required]
        [JsonProperty("listId")]
        public Guid ListId { get; set; }

        [Required]
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [Required]
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
