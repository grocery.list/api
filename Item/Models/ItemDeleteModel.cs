﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Item.Models
{
    public class ItemDeleteModel
    {
        [Required]
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [Required]
        [JsonProperty("listId")]
        public Guid ListId { get; set; }
    }
}
