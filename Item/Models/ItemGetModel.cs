﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Item.Models
{
    public class ItemGetModel
    {
        [Required]
        [JsonProperty("listId")]
        public Guid ListId { get; set; }
    }
}
