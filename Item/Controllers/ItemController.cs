﻿using Infrastructure.Repositories;
using Infrastructure.Services;
using Item.ModelBuilders;
using Item.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;

namespace Item.Controllers
{
    public class ItemController : ControllerBase
    {
        private readonly IItemRepository _itemRepository;
        private readonly IUserService _userService;
        private readonly IGroceryListRepository _groceryListRepository;
        private readonly ItemModelBuilder _itemModelBuilder;

        public ItemController(IItemRepository itemRepository, IUserService userService, IGroceryListRepository groceryListRepository, ItemModelBuilder itemModelBuilder)
        {
            _itemRepository = itemRepository;
            _userService = userService;
            _groceryListRepository = groceryListRepository;
            _itemModelBuilder = itemModelBuilder;
        }

        [Authorize]
        [HttpPost("item")]
        public async Task<IActionResult> CreateItem(ItemCreateModel model)
        {
            var user = _userService.CurrentUser;
            if (user is null) return BadRequest();

            var groceryList = await _groceryListRepository.ReadAsync(model.ListId, user);
            if (groceryList is null || model.Name.Length == 0 || model.Name.Length > 255)
                return BadRequest();

            var item = await _itemRepository.CreateAsync(model.Name, groceryList);

            var result = _itemModelBuilder.Build(item);

            return Ok(result);
        }

        [Authorize]
        [HttpGet("item")]
        public async Task<IActionResult> GetItems(ItemGetModel model)
        {
            var user = _userService.CurrentUser;
            if (user is null) return BadRequest();

            var groceryList = await _groceryListRepository.ReadAsync(model.ListId, user);
            if (groceryList is null)
                return BadRequest();

            var items = await _itemRepository.ReadCollectionAsync(groceryList);

            var result = _itemModelBuilder.Build(items);

            return Ok(result);
        }

        [Authorize]
        [HttpPatch("item/name")]
        public async Task<IActionResult> ChangeName(ItemChangeNameModel model)
        {
            var user = _userService.CurrentUser;
            if (user is null) return BadRequest();

            var groceryList = await _groceryListRepository.ReadAsync(model.ListId, user);
            if (groceryList is null)
                return BadRequest();

            var item = await _itemRepository.ReadAsync(model.Id);
            if(item is null || model.Name.Length == 0 || model.Name.Length > 255) return BadRequest();

            item = await _itemRepository.UpdateAsync(item, model.Name);

            var result = _itemModelBuilder.Build(item);

            return Ok(result);
        }

        [Authorize]
        [HttpPatch("item/completion")]
        public async Task<IActionResult> ChangeСompletion(ItemChangeCompletionModel model)
        {
            var user = _userService.CurrentUser;
            if (user is null) return BadRequest();

            var groceryList = await _groceryListRepository.ReadAsync(model.ListId, user);
            if (groceryList is null)
                return BadRequest();

            var item = await _itemRepository.ReadAsync(model.Id);
            if (item is null) return BadRequest();

            item = await _itemRepository.UpdateAsync(item, model.IsDone);

            var result = _itemModelBuilder.Build(item);

            return Ok(result);
        }

        [Authorize]
        [HttpDelete("item")]
        public async Task<IActionResult> DeleteItem(ItemDeleteModel model)
        {
            var user = _userService.CurrentUser;
            if (user is null) return BadRequest();

            var groceryList = await _groceryListRepository.ReadAsync(model.ListId, user);
            if (groceryList is null)
                return BadRequest();

            var item = await _itemRepository.ReadAsync(model.Id);
            if (item is null) return BadRequest();

            await _itemRepository.DeleteAsync(item);

            return Ok();
        }
    }
}
