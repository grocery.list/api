﻿using Infrastructure.Entity;
using Item.Models;

namespace Item.ModelBuilders
{
    public class ItemModelBuilder
    {
        public IEnumerable<ItemModel> Build(IEnumerable<IItem> items)
        {
            var itemModels = items.Select(Build);

            return itemModels;
        }
        public ItemModel Build(IItem item)
        {
            return new ItemModel
            {
                Id = item.Id,
                Name = item.Name,
                IsDone= item.IsDone
            };
        }
    }
}
