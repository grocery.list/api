using Entity;
using GroceryList.ModelBuilders;
using Infrastructure.Entity;
using Infrastructure.Repositories;
using Infrastructure.Services;
using Item.ModelBuilders;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Repositories;
using Services;
using System.Text;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddHttpContextAccessor();

ConfigureSwagger();
ConfigureDataProvider();
ConfigureRepositories();
ConfigureEntities();
ConfigureServices();
ConfigureJwt();
ConfigureModelBuilders();

var app = builder.Build();


app.UseSwagger();
app.UseSwaggerUI();


app.UseAuthorization();

app.MapControllers();

app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().SetPreflightMaxAge(TimeSpan.MaxValue));

app.Run();

void ConfigureDataProvider()
{
    var connectionString = builder.Configuration.GetConnectionString("Grocery");

    builder.Services.AddDbContext<ApplicationDbContext>(options =>
    {
        options.UseSqlServer(connectionString);
    });
}

void ConfigureJwt()
{
    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = builder.Configuration["Jwt:Issuer"],
            ValidAudience = builder.Configuration["Jwt:Audience"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:SecurityKey"]))
        };
    });
}

void ConfigureRepositories()
{
    builder.Services.AddScoped<IUserRepository, UserRepository>();
    builder.Services.AddScoped<IUserRefreshTokenRepository, UserRefreshTokenRepository>();

    builder.Services.AddScoped<IGroceryListRepository, GroceryListRepository>();

    builder.Services.AddScoped<IItemRepository, ItemRepository>();
}
void ConfigureSwagger()
{
    builder.Services.AddSwaggerGen(swagger =>
    {
        swagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
        {
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey,
            Scheme = "Bearer",
            BearerFormat = "JWT",
            In = ParameterLocation.Header,
            Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
        });
        swagger.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {
                new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    }
                },
                new string[] { }

            }
        });
    });
}

void ConfigureEntities()
{
    builder.Services.AddTransient<IUser, UserEntity>();
    builder.Services.AddTransient<IUserRefreshToken, UserRefreshTokenEntity>();

    builder.Services.AddTransient<IGroceryList, GroceryListEntity>();

    builder.Services.AddTransient<IItem, ItemEntity>();
}

void ConfigureServices()
{
    builder.Services.AddScoped<IUserService, UserService>();
}

void ConfigureModelBuilders()
{
    builder.Services.AddScoped<GroceryListModelBuilder>();
    builder.Services.AddScoped<ItemModelBuilder>();
}